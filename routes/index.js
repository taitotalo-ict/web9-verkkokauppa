var express = require('express');
var router = express.Router();
const { Tuote } = require('../models/mallit');

/* GET home page. */
router.get('/', async (req, res, next) => {
  let user = req.session.user ? req.session.user : null;

  tuotteet = await Tuote.findAll();
  let tuoteIndex = Math.floor(Math.random()*tuotteet.length)
  tuote = tuotteet[tuoteIndex];

  res.render('index', { title: 'Etusivu', user: user, tuote: tuote });
});

router.get('/yhteystiedot', (req, res, next) => {
  let user = req.session.user ? req.session.user : null;

  res.render('yhteystiedot', { title: 'Yhteystiedot', user: user });
});

router.get('/tietosuojaseloste', (req, res, next) => {
  let user = req.session.user ? req.session.user : null;

  res.render('tietosuojaseloste', { title: 'Tietouosuojaseloste', user: user });
});

module.exports = router;
