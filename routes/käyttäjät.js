var express = require('express');
var router = express.Router();
const { Asiakas } = require('../models/mallit');
const bcrypt = require('bcrypt');

// Form input validation
const { body, validationResult } = require('express-validator');

function login(session, user) {
  session.user = {
    id: user.id,
    etunimi: user.etunimi ,
    isAdmin: user.isAdmin
  }
}

// Create a new object with better organization for the form's error messages
let errorMessages = errors => {
  messages = {}
  errors.errors.forEach(error => {
      if (messages[error.param]) {
          messages[error.param].push(error.msg);               
      } else {
          messages[error.param] = [error.msg];
      }
  });
  return messages;
}



router.get('/rekisterointi', (req, res, next) => {
  if (req.session.user) {
    res.redirect('/');
  }
  res.render('rekisteröinti', { title: 'Käyttäjän rekisteröinti', lomake: {} });
});

// Helper function to validate registration form input
validateRegistrationInput = [
  // Username is not empty, min length of 4, it's trimmed and lowercasered
  body('username')
    .notEmpty().withMessage('Käyttäjätunnus on pakollinen')
    .trim()
    .isLength({min: 4}).withMessage('Käyttäjätunnuksen minimi pituus on 4 merkkiä')
    .isLength({max: 150}).withMessage('Käyttäjätunnuksen maksimi pituus on 150 merkkiä')
    .toLowerCase(),
  // Username must use only latin letter and numbers.
  body('username').custom( value => {
    if (value.match(/^[0-9a-z]+$/)) {
      return true
    } else {
      return Promise.reject('Kielettyä merkkiä käyttäjätunnuksessa. Vain a-z ja 0-9 salittu.')
    }
  }),
  // Username is not already taken
  body('username').custom( async käyttäjätunnus => {
    if (await Asiakas.findOne({ where: {käyttäjätunnus: käyttäjätunnus}})) {
      return Promise.reject('käyttäjätunnus on jo olemassa');
    } else {
      return true;
    }
  }),
  // First name is not empty and it's escaped
  body('first_name')
      .trim()
      .escape(),
  // Same to last name
  body('last_name')
      .trim()
      .escape(),
  // email must be an email and it's trimmed
  body('email')
      .trim()
      .isEmail(),
  // Email is not already taken
  body('email').custom( async email => {
      if (await Asiakas.findOne({ where: {email: email}})) {
        return Promise.reject('Sähköpostiosoite on jo olemassa');
      } else {
        return true;
      }
    }),
  // password must be at least 8 chars long
  body('password1')
      .isLength({ min: 8 }).withMessage('Salasanan minimi pituus on 8 merkkiä.'),
  body('password1').custom((value, { req }) => {
      if (value !== req.body.password2) {
          throw new Error('Salasanat eivät täsmä');
      } else {
          return true
      }
  })
];


router.post('/rekisterointi', validateRegistrationInput, async (req, res, next) => {
  if (req.session.user) {
    res.redirect('/');
  }

  // Process validation errors
  const errors = errorMessages(validationResult(req));

  if (Object.keys(errors).length == 0) {
    let isAdmin = (await Asiakas.count() == 0);
  
    // Prepare password encryption
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(req.body.password1, salt);
  
    let asiakas = await Asiakas.create({
      käyttäjätunnus: req.body.username,
      etunimi: req.body.first_name,
      sukunimi: req.body.last_name,
      email: req.body.email,
      salasana: hash,
      isAdmin: isAdmin
    });
    // login
    login(req.session, asiakas);
    res.redirect('/');
  } else {
    res.render('rekisteröinti', { title: 'Käyttäjän rekisteröinti', lomake: req.body, errors: errors, user: null });
  }
});

router.get('/kirjautuminen', (req, res, next) => {
  if (req.session.user) {
    res.redirect('/');
  }
  res.render('kirjautuminen', { title: 'Kirjaudu sisään', lomake: {} });
});

router.post('/kirjautuminen', async (req, res, next) => {
  if (req.session.user) {
    res.redirect('/');
  }
  // Tarkista tietoa lomakkeesta
  let asiakas = await Asiakas.findOne({where: {käyttäjätunnus: req.body.username}});
  console.log(asiakas);
  if (asiakas) {
    // Check password encryption
    if (await bcrypt.compare(req.body.password, asiakas.salasana )) {
      // login
      login(req.session, asiakas);
      res.redirect('/');
    }
  }

  res.render('kirjautuminen', { title: 'Kirjaudu sisään', lomake: req.body});
});

router.get('/kirjauduulos', (req, res, next) => {
  if (req.session.user) {
    req.session.destroy();
  }
  res.redirect('/');
});

module.exports = router;
