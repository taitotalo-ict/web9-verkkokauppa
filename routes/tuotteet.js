const express = require('express');
const router = express.Router();
const path = require('path');

const { Tuote } = require('../models/mallit');
const fs = require('fs');

// Form input validation
const { body, validationResult } = require('express-validator');

// For image upload
var multer  = require('multer')
var upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => cb(null, uploadsFolder),
        filename: (req, file, cb) => {
          const index = file.originalname.lastIndexOf('.');
          const filename = file.originalname.substring(0,index);
          const ext = file.originalname.substring(index);
          return cb(null, `${filename}-${Date.now()}-${Math.round(Math.random() * 1E9)}${ext}`);
        }
    }),
    fileFilter: (req, file, cb) => {
        let acceptedMimeTypes = ["image/png", "image/jpg", "image/jpeg", "image/webp"];
        cb(null, acceptedMimeTypes.includes(file.mimetype))
    }
});

// Create a new object with better organization for the form's error messages
let errorMessages = errors => {
  messages = {}
  errors.errors.forEach(error => {
      if (messages[error.param]) {
          messages[error.param].push(error.msg);               
      } else {
          messages[error.param] = [error.msg];
      }
  });
  return messages;
}

router.get('/', async (req, res, next) => {
    let user = req.session.user ? req.session.user : null;

    let tuotteet = await Tuote.findAll();
  
    res.render('tuotteet', { title: 'Tuotteet', user: user, tuotteet: tuotteet });
});

router.get('/tuote/:id', async (req, res, next) => {
    let user = req.session.user ? req.session.user : null;

    let tuote = await Tuote.findByPk(req.params.id);

    res.render('tuote_yksityiskohdat', { title: 'Tuotteen yksityiskohdat', user: user, tuote: tuote } );
});

router.get('/uusi', async (req, res, next) => {
  let user = req.session.user ? req.session.user : null;

  if (!user || !user.isAdmin) {
    res.redirect('/');
  }
  
  res.render('lisätätuote', { title: 'Lisätä Tuote', user: user, errors: {}, lomake: {} });
});


// Helper function to validate product form input
validateProducInput = [
  // Product name is not empty, min length of 4, it's trimmed and escaped
  body('nimi')
      .not().isEmpty().withMessage('Tuotteen nimi on pakollinen')
      .trim().escape()
      .isLength({min: 4}).withMessage('Tuotteen nimi minimi pituus on 4 merkkiä')
      .isLength({max: 100}).withMessage('Käyttäjätunnuksen maksimi pituus on 100 merkkiä'),
  // Product name is not already taken
  body('nimi').custom( async nimi => {
      if (await Tuote.findOne({ where: {nimi: nimi}})) {
          return Promise.reject('Tuotteen nimi on jo olemassa');
      } else {
          return true;
      }
  }),
  // Product description is not empty and it's escaped
  body('lisätiedot')
      .not().isEmpty().withMessage('Tuotteen lisätiedot ovat pakollista')
      .trim()
      .escape(),
  // Price must be a positive number (decimals allowed)
  body('hinta').custom( hinta => {
      hinta = hinta.replace(',','.');
      if (+hinta && +hinta > 0) {
          return true;
      } else {
          return Promise.reject('Hinta pitää olla positiivinen luku');
      }
  }),
  // Quantity must be a positive integer (no decimals allowed)
  body('määrä').custom( määrä => {
      if (määrä.match(/^[0-9]+$/)) {
          return true;
      } else {
          return Promise.reject('Määrä pitää olla positiivinen kokonaisluku');
      }
  })
];


router.post('/uusi', upload.single('kuva'), validateProducInput, async (req, res, next) => {
  let user = req.session.user ? req.session.user : null;

  if (!user || !user.isAdmin) {
    res.redirect('/');
  }

  // Process validation errors
  const errors = errorMessages(validationResult(req));

  const success = (Object.keys(errors).length == 0);

  if (success) {
    await Tuote.create({
      nimi: req.body.nimi,
      lisätiedot: req.body.lisätiedot,
      hinta: req.body.hinta,
      kuva: req.file.filename,
      määrä: req.body.määrä
    });
  } else {
    // Remove file
    fs.unlink(path.join(uploadsFolder, req.file.filename), (err)=>{console.log(err);});
  }

  res.render('lisätätuote', { title: 'Lisätä Tuote', user: user, errors: errors, lomake: success?{}:req.body, success: success });
});

module.exports = router;