const { Sequelize, Model, DataTypes } = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite'
});

/* Asiakas malli */
const Asiakas = sequelize.define('asiakas', {
    käyttäjätunnus : { type: DataTypes.STRING(50),  allowNull: false, unique: true        },
    etunimi        : { type: DataTypes.STRING(50)                                         },
    sukunimi       : { type: DataTypes.STRING(50)                                         },
    email          : { type: DataTypes.STRING(50),  allowNull: false, unique: true        },
    salasana       : { type: DataTypes.STRING(100), allowNull: false                      },
    isAdmin        : { type: DataTypes.BOOLEAN,     allowNull: false, defaultValue: false }
  },{
    tableName: 'asiakkaat'
  });

/* Tuote malli */
const Tuote = sequelize.define('tuote', {
  nimi       : { type: DataTypes.STRING(100),  allowNull: false, unique: true },
  lisätiedot : { type: DataTypes.TEXT                                         },
  hinta      : { type: DataTypes.DECIMAL(6,2), allowNull: false               },
  määrä      : { type: DataTypes.INTEGER,      allowNull: false               },
  kuva       : { type: DataTypes.STRING(100)                                  }
},{
  tableName: 'Tuotteet'
});


(async () => { await sequelize.sync({ alter: true }) })();


module.exports = { Asiakas, Tuote };
