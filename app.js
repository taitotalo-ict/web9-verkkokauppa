var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var käyttäjätRouter = require('./routes/käyttäjät');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Set 'public' as static folder
global.publicFolder = path.join(__dirname, 'public');
app.use(express.static(publicFolder));

// Set uploads folder
const uploads = 'uploads';
global.uploadsFolder = path.join(publicFolder, uploads);
app.locals.uploadsFolder = path.join('/',uploads,'/');

////////////////////////////////
/////  Session management  /////
////////////////////////////////
// express-session is required to handle user sessions
var session = require('express-session');

// Option 1: Sessions are stored in memory:
// This is actually not needed, as it is the default. Just omit store param in session module initialization.
// var MemoryStore = session.MemoryStore;
// var myStore = new MemoryStore({
//   checkPeriod: 86400000 // prune expired entries every 24h
// });

// Option 2: Session management with sqlite database:
var Sequelize = require("sequelize");
var sequelizeSession = new Sequelize({
  dialect: "sqlite",
  storage: "./session.sqlite",
});
var SequelizeStore = require("connect-session-sequelize")(session.Store);
var myStore = new SequelizeStore({
  db: sequelizeSession,
});
myStore.sync();

// Initialize session module using selected store management above and set express to use it.
app.use(session({
  secret: 'MySecret',
  store: myStore,
  resave: false,
  saveUninitialized: false
}));

app.use('/', indexRouter);
app.use('/kayttajat', käyttäjätRouter);
app.use('/tuotteet', require('./routes/tuotteet'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// Helper functions
app.locals.truncate = (teksti, pituus) => {
  if (teksti.length > pituus) {
    return teksti.slice(0, pituus) + '...';
  } else {
    return teksti;
  }
}

app.locals.hinta = (hinta) => {
  return String(hinta.toFixed(2)).replace('.',',');
}

app.locals.parseDate = (date) => {
  day = date.getDate();
  month = date.getMonth() + 1;
  year = date.getFullYear();
  min = date.getMinutes();
  hour = date.getHours();
  
  return `${day}.${month}.${year} ${hour}:${min}`
}


module.exports = app;
